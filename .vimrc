set number " 设置行号
syntax on " 设置自动语法高亮 
set encoding=utf8 " 设置utf编码
set autoindent " 设置自动缩进
set tabstop=2 " 设置tab宽度
set shiftwidth=2 " 设置 >> 命令移动的宽度
set softtabstop=2 " 设置back space按键删除的空格长度
set nocompatible " 关闭vi兼容模式
filetype plugin on " 插件能获取文件类型

" 插件列表
call plug#begin()

Plug 'mhinz/vim-startify' " 欢迎界面
Plug 'scrooloose/nerdtree' " 文件树
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } } " 搜索
Plug 'junegunn/fzf.vim'
Plug 'Yggdroot/indentLine' " 缩进辅助线
Plug 'scrooloose/nerdcommenter' " 注释工具
Plug 'jiangmiao/auto-pairs' " 括号配对
Plug 'frazrepo/vim-rainbow' " 括号高亮

call plug#end()

" 文件树插件映射
nmap <C-n> :NERDTreeToggle<CR> " ctrl+n 打开或关闭文件树
nmap <C-m> :NERDTreeFind<CR> " ctrl+f 文件树定位到当前文件
" 搜索插件映射
nmap <c-f> :Files<CR> " 文件搜索
nmap <c-g> :Ag<CR> " 全局搜索
